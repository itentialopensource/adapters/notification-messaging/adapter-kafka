
## 0.3.21 [06-30-2023]

* Add support for multiple partitions in an array

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!27

---

## 0.3.20 [06-30-2023]

* Updated to use http.request

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!26

---

## 0.3.19 [06-07-2023]

* support for separate sasl for producer and consumer

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!25

---

## 0.3.18 [06-07-2023]

* support for separate sasl for producer and consumer

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!25

---

## 0.3.17 [05-25-2023]

* Kafka Edits

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!24

---

## 0.3.16 [05-25-2023]

* Kafka Edits

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!24

---

## 0.3.15 [04-12-2023]

* Kafka Edits

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!24

---

## 0.3.14 [04-12-2023]

* Kafka Edits

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!24

---

## 0.3.13 [04-12-2023]

* Kafka Edits

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!24

---

## 0.3.12 [04-07-2023]

* updated to created separate topic file for each instance of adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!22

---

## 0.3.11 [09-29-2022]

* Added patch for SCRAM-SHA-256/512 support.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!20

---

## 0.3.10 [09-29-2022]

* Fixed messages filtering. Allow regular expressions to be used as filters.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!18

---

## 0.3.9 [08-22-2022]

* Fixed invalid input variable in `subscribe` task.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!19

---

## 0.3.8 [08-05-2022]

* Prevent emitting duplicated messages to event subscribers.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!17

---

## 0.3.7 [08-05-2022]

* Replaced 'writeTime' prop with 'interval_time' prop.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!16

---

## 0.3.6 [08-05-2022]

* Fixed logging for consumed/dropped messages.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!15

---

## 0.3.5 [08-05-2022]

* Improvments to handling 'offsetOutOfRange' errors.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!14

---

## 0.3.4 [08-01-2022]

* Added kafka-node logger.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!13

---

## 0.3.3 [08-01-2022]

* Added hostList property to adapter's config.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!12

---

## 0.3.2 [07-29-2022]

* Offest changes

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!11

---

## 0.3.1 [01-18-2021]

* Patch/adapt 401

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!10

---

## 0.3.0 [12-01-2020]

* fix the partition and offset on the subscribe and subscribeAvro

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!9

---