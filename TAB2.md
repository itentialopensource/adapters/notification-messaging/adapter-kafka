# Kafka

## Table of Contents 

  - [Specific Adapter Information](#specific-adapter-information) 
    - [Authentication](#authentication) 
    - [Sample Properties](#sample-properties) 
    - [Swagger](#swagger) 
  - [Generic Adapter Information](#generic-adapter-information) 

## Specific Adapter Information
### Authentication

This document will go through the steps for authenticating the Kafka adapter with. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>. 

#### Library Authentication
The Kafka adapter supports SASL Authentication for Kafka server. If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to a Kafka server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for the ```properties.client.sasl``` field
```json
"sasl": {
  "mechanism": "PLAIN",
  "username": "username",
  "password": "password"
}
```
For more details on sasl authentication follow the PROPERTIES.md
4. Restart the adapter. If your properties were set correctly, the adapter should go online. 

#### Troubleshooting
- Make sure you copied over the correct username and password.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Investigate the logs
- Credentials should be ** masked ** by the adapter so make sure you verify the username and password - including that there are erroneous spaces at the front or end.
### Sample Properties

Sample Properties can be used to help you configure the adapter in the Itential Automation Platform. You will need to update connectivity information such as the host, port, protocol and credentials.

```json
  "properties": {
    "host": "localhost",
    "port": 9092,
    "registry_url": "",
    "topics": [],
    "interval_time": 5000,
    "stub": false,
    "client": {
      "connectTimeout": 10000,
      "requestTimeout": 30000,
      "autoConnect": true,
      "connectRetryOptions": {},
      "idleConnection": 300000,
      "reconnectOnIdle": true,
      "maxAsyncRequests": 10,
      "sslOptions": {},
      "sasl": {
        "mechanism": "PLAIN",
        "username": "username",
        "password": "password"
      }
    },
    "producer": {
      "requireAcks": 1,
      "ackTimeoutMs": 100,
      "partitionerType": 0
    },
    "consumer": {
      "groupId": "kafka-node-group",
      "autoCommit": false,
      "autoCommitIntervalMs": 5000,
      "fetchMaxWaitMs": 100,
      "fetchMinBytes": 1,
      "fetchMaxBytes": 1048576,
      "fromOffset": true,
      "encoding": "utf8",
      "keyEncoding": "utf8"
    }
  }
```
### Swagger

Note: The content for this section may be missing as its corresponding .json file is unavailable. This sections will be updated once adapter-openapi.json file is added.
## [Generic Adapter Information](https://gitlab.com/itentialopensource/adapters/adapter-kafka/-/blob/master/README.md) 

