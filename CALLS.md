## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The following is a brief summary of the calls.

### Generic Adapter Calls

```json
healthCheck(callback)
Insures that the adapter can successfully create a Kafka client.
```

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Kafka. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

#### SendMessage Method

The sendMessage adapter task is used in order to send a message to the specified kafka topic. An example payload would be as follows: 

```json 
[
    {
        "topic": "topic-name",
        "messages": "message"
    }
]
```
Additional details may be provided in this payload, including a `key` property if using a keyed partitioner. 

## Triggering jobs in IAP from a kafka message

Once you have the adapter configured and online in an IAP instance, you can create triggers in Operations Manager to kick off jobs when a kafka message is consumed. The adapter listens for messages on the subscribed topics and publishes to an event. This event is picked up by the Operations Manager. If you have created triggers for this specific topic, a job will get triggered. For a step-by-step example follow the next sub-section.

### Listen to a topic called test-topic and trigger a workflow called test

1. Subscribe to partition 0 of test-topic. Add this to your adapter service config. To see different options of topic properties see PROPERTIES.md

```json
"topics": [
  {
    "name": "test-topic",
    "always": true,
    "partition": 0,
    "subscriberInfo": [
      {
        "subname": "default",
        "filters": [],
        "rabbit": "kafka",
        "throttle": {}
      }
    ]
  }
],
```

Note the rabbit key in the config is set to kafka. The adapter will by default send any message on test-topic to a Rabbit queue named test-topic. Also note that the filter array can be left empty to consume all messages on partion 0 of test-topic.

2. Create an automation in Operations Manager

Create a new automation, and add a trigger.

When selecting the Event for your trigger, make sure to select test-topic from the drop down list. This is critical as the adpater will be sending the message on this queue. You can add filtering in the trigger by providing a Payload Schema Filter.

After saving the trigger, select the workflow you want to trigger. Save the Automation and view all jobs to see a job getting triggered as soon as a message is published on test-topic partition 0.