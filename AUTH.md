## Authenticating Kafka Adapter 

This document will go through the steps for authenticating the Kafka adapter with. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>. 

### Library Authentication
The Kafka adapter supports SASL Authentication for Kafka server. If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to a Kafka server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for the ```properties.client.sasl``` field
```json
"sasl": {
  "mechanism": "PLAIN",
  "username": "username",
  "password": "password"
}
```
For more details on sasl authentication follow the PROPERTIES.md
4. Restart the adapter. If your properties were set correctly, the adapter should go online. 

### Troubleshooting
- Make sure you copied over the correct username and password.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Investigate the logs
- Credentials should be ** masked ** by the adapter so make sure you verify the username and password - including that there are erroneous spaces at the front or end.