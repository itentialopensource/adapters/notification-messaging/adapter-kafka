# Kafka

Vendor: Apache Kafka
Homepage: https://kafka.apache.org/

Product: Kafka 
Product Page: https://kafka.apache.org/

## Introduction

We classify Kafka into the Notifications domain as Kafka is used to send message between systems. This version of the Kafka adapter uses an older library. There is also a Kafkav2 adapter that uses a more modern Kafka library.

Apache Kafka is an open-source distributed event streaming platform used by thousands of companies for high-performance data pipelines, streaming analytics, data integration, and mission-critical applications.

"Deliver messages at network limited throughput using a cluster of machines with latencies as low as 2ms."
"Store streams of data safely in a distributed, durable, fault-tolerant cluster."

## Why Integrate
The Kafka adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Kafka. With this adapter you have the ability to perform operations with Kafka on items such as:

- Produce/Publish messages onto message topics for others to consume
- Listen, receive and process messages placed on topics by other systems

## Additional Product Documentation
The [Kafka Documentation](https://kafka.apache.org/documentation/)
The [Kafka Node Library Documentation](https://www.npmjs.com/package/kafka-node)