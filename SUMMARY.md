## Overview
This adapter is used to integrate the Itential Automation Platform (IAP) with the Kafka System. The adapter utilizes the kafka-node client to integrate with Apache Kafka. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

>**Note**: It is possible that some integrations will be supported through the Kafka adapter while other integrations will not.

Itential provides information on all of its product adapters in the Customer Knowledge Base. Information in the <a href="https://itential.atlassian.net/servicedesk/customer/portals" target="_blank">Customer Knowledge Base</a> is consistently maintained and goes through documentation reviews. As a result, it should be the first place to go for information.

For opensourced and custom built adapters, the ReadMe is a starting point to understand what you have built, provide the information for you to be able to update the adapter, and assist you with deploying the adapter into IAP.