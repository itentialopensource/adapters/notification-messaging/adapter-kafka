
## Troubleshooting the Adapter

### Connectivity Issues

1. Verify the adapter properties are set up correctly.

```json
Go into the Itential Platform GUI and verify/update the properties
```

2. Verify there is connectivity between the Itential Platform Server and Kafka Server.

```json
ping the ip address of Kafka server
try telnet to the ip address port of Kafka
use kafka cli tool to connect to kafka broker
```

To understand kafka client to broker interaction, follow [this](https://www.confluent.io/blog/kafka-client-cannot-connect-to-broker-on-aws-on-docker-etc/) link

3. Verify the credentials provided for Kafka.

```json
use kafka cli tool to connect to kafka broker using the same credentials
```


### Functional Issues

Adapter logs are located in `/var/log/pronghorn`. In older releases of the Itential Platform, there is a `pronghorn.log` file which contains logs for all of the Itential Platform. In newer versions, adapters are logging into their own files.