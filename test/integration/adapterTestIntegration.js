/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const mocha = require('mocha');
const path = require('path');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const stub = true;
const isRapidFail = false;
const isSaveMockData = false;
const attemptTimeout = 10000;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
const host = 'localhost';
const username = 'username';
const password = 'password';
const protocol = 'mongodb';
const port = 9092;
const sslenable = false;
const sslinvalid = false;

const randExt = `${Math.random()}`;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-kafka',
      type: 'Kafa',
      interval_time: 5000,
      stub: true,
      properties: {
        host: 'localhost',
        port: 9092
      }
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

// require the adapter that we are going to be using
const Kafka = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Kafka Adapter Test', () => {
  describe('Kafka Class Tests', () => {
    const a = new Kafka(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        assert.notEqual(null, a);
        assert.notEqual(undefined, a);
        const check = global.pronghornProps.adapterProps.adapters[0].id;
        assert.equal(check, a.id);
        done();
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      if (!stub) {
        it('should get connected', (done) => {
          a.connect();
          assert.equal(true, a.alive);
          done();
        }).timeout(attemptTimeout);
      }
    });

    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */
    const topics = [{
      topic: `test${randExt}`,
      partitions: 1,
      replicationFactor: 1
    }];
    describe('#createTopics', () => {
      if (!stub) {
        it('should create topics', (done) => {
          try {
            a.createTopics(topics, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const payloads = [
      { topic: topics[0].topic, messages: `hello world${randExt}`, partitions: 1 }
    ];
    describe('#sendMessage', () => {
      if (!stub) {
        it('should send message', (done) => {
          try {
            a.sendMessage(payloads, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const subscribeTopicsConsumer = [topics[0].topic];
    describe('#subscribe', () => {
      if (!stub) {
        it('should add topics', (done) => {
          try {
            a.subscribe(subscribeTopicsConsumer, 0, 0, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const subscribeAvroTopicsConsumer = [topics[0].topic];
    describe('#subscribeAvro', () => {
      if (!stub) {
        it('should add topics', (done) => {
          try {
            a.subscribeAvro(subscribeAvroTopicsConsumer, 0, 0, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const topicToSetOffset = topics[0].topic;
    const partition = 1;
    const offset = 0;
    describe('#setOffset', () => {
      if (!stub) {
        it('should set offset', (done) => {
          try {
            a.setOffset(topicToSetOffset, partition, offset, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const topicsToPause = [topics[0].topic];
    describe('#pauseTopics', () => {
      if (!stub) {
        it('should pause Topics', (done) => {
          try {
            a.pauseTopics(topicsToPause, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const topicsToResume = [topics[0].topic];
    describe('#resumeTopics', () => {
      if (!stub) {
        it('should resume Topics', (done) => {
          try {
            a.resumeTopics(topicsToResume, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    describe('#pauseConsumer', () => {
      if (!stub) {
        it('should pause consumer', (done) => {
          try {
            a.pauseConsumer((data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    describe('#resumeConsumer', () => {
      if (!stub) {
        it('should resume consumer', (done) => {
          try {
            a.resumeConsumer((data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    describe('#commitOffset', () => {
      if (!stub) {
        it('should commit offset', (done) => {
          try {
            a.commitOffset((data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const unsubscribeTopicsConsumer = [topics[0].topic];
    describe('#unsubscribe', () => {
      if (!stub) {
        it('should remove topics', (done) => {
          try {
            a.unsubscribe(unsubscribeTopicsConsumer, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const closeForce = false;
    describe('#closeConsumer', () => {
      if (!stub) {
        it('should close consumer', (done) => {
          try {
            a.closeConsumer(closeForce, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });
  });
});
